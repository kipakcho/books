# Books

## Updates to this project

May 6, 2024 - Added new book "Man of God", which is a compilation of "The Humble Man", "The New Man", and "The True Man" into one volume. Broken down into chapters, no subheadings. Revised, updated, deleted some parts, added others for more clarity. My opinion is that this three in one volume is better than the other three individual volumes. 2024-05-06

April 22, 2024 - Added new book "The True Man"  - 2024-04-22

April 6, 2024 - Added recently completed book "The New Man" to book collection.

March 24, 2024 - Added recently completed book "The Humble Man" to this book collection.

November 25, 2023 - Updated file names for brevity and accuracy. 2023-11-25

November 24, 2023 - Updated Romans (Individual and Complete), made minor correction in text. 2023-11-24

November 16, 2023 - removed Bibles. I have another repo for Bibles where Bibles can be downloaded. 2023-11-16

October 15, 2023 - Updated the Complete Zip with an ODT version as well as the PDF file. 

October 9, 2023 - Updated Matthew in the individual files. Matthew was missing a couple of chapters! I've updated Matthew with some new formatting and replaced those chapters with the original text from the "complete" version of the New Testament commentary. The complete version still had the original chapters included. Also, noticed that the Complete zip version only has a PDF version of the commentary. I need to update this at some point with the "odt" version as well.
 
December 10, 2021 - Renamed bible commentary zip files to shorter names.

June 27, 2021 - Added New Testament commentary in one completed PDF book.

June 22, 2021 - Added updated Revelation to zip; renamed zip files; separated 'He Speaks' book from New Testament commentary books; separated the Holy Bible PDF into separate zip file. Upcoming soon! One complete book containing the New Testament commentary in one complete PDF book.

June 19, 2021 - Added updated Jude to zip; removed outdated version.

June 19, 2021 - Added updated First John through Third John to zip; removed outdated version.

June 17, 2021 - Added updated Second Peter to zip; removed outdated
version. 

June 16, 2021 - Added updated First Peter to zip; removed outdated
version. Removed folders, leaving only the ZIP for anyone to download to
their own PC. Don't really need all the folders in this repo.

June 14, 2021 - Added updated James to zip; removed outdated version.

June 11, 2021 - Added updated Hebrews to zip; removed outdated version.

June 1, 2021 - Added updated Philemon to zip; removed outdated version.

May 31, 2021 - Added updated Titus to zip; removed outdated version.

May 28, 2021 - Added updated Second Timothy to zip; removed outdated version.

May 26, 2021 - reformatted all books using a uniform template. 
               removed all old copies of books, leaving the most up to date.

May 26, 2021 - added updated First Timothy; removed all old, outdated
                versions of books from the zip file. Kept only the newest
                versions in the zip file.

May 25, 2021 - added updated Second Thessalonians

May 24, 2021 - added updated First Thessalonians

May 19, 2021 - added updated version of Colossians.

May 14, 2021 - added unzipped folders for those updated in 2021 (no PDF). Download 
 				zip file to get all of the PDFs. 

May 13, 2021 - added updated version of Philippians

May 10, 2021 - added updated version of Ephesians

May 4, 2021 - added updated version of Galatians

May 1, 2021 - added an updated version of Second Corinthians

April 27, 2021 with updated version of First Corinthians

April 2021 with updated version of Romans.

March 21, 2021 with updated version of Acts

February 25, 2021 with a revised edition of John 

February 7, 2021 with a revised edition of Luke

January 24, 2021 with a revised edition of Mark. 

January 20, 2021 with a revised edition of Matthew.

Created September 2020
